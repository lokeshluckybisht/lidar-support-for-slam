# Lidar obstacle detection

In this , we will separate the roads from obstacles and then cluster each obstacle and then make bounding boxes around those obstacles. in addition to this, filtering is also applied so as to decrease the load on processor. Everything can be tuned with the help of parameters later.

## Following are the steps for the pipeline
- Read a PCD file.
- Filter the raw LIDAR data 
- Segment the filtered data to identify road and objects
- Clustering: from objects identify the clusters.
- For each cluster append a bounding box. 
- Rendering: Render the objects and road to the viewer. 

## Installation

### Linux Ubuntu 16

Install PCL, C++

The link here is very helpful, 
https://larrylisky.com/2014/03/03/installing-pcl-on-ubuntu/

A few updates to the instructions above were needed.

* libvtk needed to be updated to libvtk6-dev instead of (libvtk5-dev). The linker was having trouble locating libvtk5-dev while building, but this might not be a problem for everyone.

* BUILD_visualization needed to be manually turned on, this link shows you how to do that,
http://www.pointclouds.org/documentation/tutorials/building_pcl.php

##Execution

Go to the build folder and run the following commands.
1. make
2. Once the make is complete, run the executable ./environment

You can pan or zoom the output. This is a rough output and can be fine tuned, which is just a matter of parameters.

